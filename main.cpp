#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{   
    const unsigned int DIM1 = 3;
    const unsigned int DIM2 = 5;

    double A[] = {5, 4, 2, 41, 10, 6, 12};
    double Summa;

    cout << "Оригинал: [ ";
    for (int i = 0; i < 7; ++i) {
        cout << A[i] << " " ;
    }
    cout<< "]" << endl;

    Summa = 0;
    for(int i = 0; i < 7; i++)
        Summa = Summa + A[i];

    cout << "Сумма: " << Summa << endl;
    cout << "Среднее: " << Summa/7 << "\n" << endl;

    int ary[DIM1][DIM2] = {
        { 1, 2, 3, 4, 5 },
        { 2, 4, 6, 8, 10 },
        { 3, 6, 9, 12, 15 }
    };  
    
    cout << "Оригинал: " << endl;

    for (int i = 0; i < DIM1; i++) {
      for (int j = 0; j < DIM2; j++) {
          cout << setw(4) << ary[i][j];
      }
      cout << endl;
    }

    for (int i=0; i < DIM1; i++)
    {
      for (int j=0; j < DIM2; j++)
      {
        Summa += ary[i][j];
      }
    }

    cout << "Сумма: " << Summa <<endl;
    cout << "Среднее: " << Summa/15 << "\n" << endl;

  return 0;
}
